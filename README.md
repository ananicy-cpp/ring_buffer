# Installation

```
mkdir -p build
cd build
cmake ..
sudo cmake --install .
```

# Usage

## Add to dependencies
In your CMake project, add:
```cmake
find_package(ring_buffer 0.1 REQUIRED)
target_link_libraries(<YOUR_TARGET> PRIVATE ring_buffer)
```

## Code usage

```cpp
int main() {
	ring_buffer<float, 32> buffer;
	buffer.push(3.14);

	// ...

	std::cout << buffer.pop_get() << std::endl;

	return EXIT_SUCCESS;
}
```

