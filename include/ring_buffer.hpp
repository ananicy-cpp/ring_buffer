#pragma once
#include <atomic>
#include <type_traits>
#include <array>
#include <mutex>
#include <optional>
#include <vector>
#include <limits>
#include <algorithm>
#include <concepts>

namespace nextlib {

    template<typename T, std::size_t Size>
    class ring_buffer {
    private:
        std::array <std::atomic<T>, Size> container{T{}};
        std::atomic <std::size_t> write_cursor = 0;
        std::atomic <std::size_t> read_cursor = 0;
        std::mutex read_mutex{};

        std::atomic <std::size_t> lost_data = 0;


        static_assert(std::atomic<std::size_t>::is_always_lock_free);
        static_assert(std::atomic<T>::is_always_lock_free,
        "T must be lock free to avoid some kind of race conditions");

    public:
        ring_buffer() = default;

        bool push(const T &value);
        bool push(const volatile T &value);

        std::optional <T> pop_get();

        [[nodiscard]] std::size_t capacity() const { return Size; }

        [[nodiscard]] std::size_t size() const;

        [[nodiscard]] std::size_t lost_element_count();

        void debug();
    };

}

#include "impl/ring_buffer-impl.hpp"