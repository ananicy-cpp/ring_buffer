#pragma once

#include <ring_buffer.hpp>

#ifndef DEBUG

template<class... Args>
    void print(Args &&... args) {}

#else
#include <fmt/format.h>
#include <fmt/ranges.h>

using fmt::print;
#endif

namespace nextlib {

    static inline auto mod(auto k, auto n) {
      return ((k %= n) < 0) ? k + n : k;
    }

#define MAX(a, b) \
  (((a) > (b)) ? (a) : (b))


    template<typename T, std::size_t Size>
    bool ring_buffer<T, Size>::push(const T &value) {
      std::size_t index = mod(write_cursor.fetch_add(1), Size);

      container[index] = value;

      return true;
    }

    template<typename T, std::size_t Size>
    bool ring_buffer<T, Size>::push(volatile const T &value) {
      std::size_t index = mod(write_cursor.fetch_add(1), Size);

      container[index] = value;

      return true;
    }

    template<typename T, std::size_t Size>
    std::optional <T> ring_buffer<T, Size>::pop_get() {
      std::lock_guard <std::mutex> lock(read_mutex);

      int64_t w = write_cursor, r = read_cursor, s = Size;
      int64_t distance = w - r;
      int64_t lost = MAX(distance - s, 0);
      lost_data += lost;
      read_cursor += lost;

      if (distance <= 0)
        return {};

      std::size_t index = mod(r, Size);
      T val = container[index].exchange(T{});
      read_cursor++;

      return val;
    }


    template<typename T, std::size_t Size>
    void ring_buffer<T, Size>::debug() {
      print("R: {}, W: {}, Size: {}, LostData: {}, Container: {}\n",
            read_cursor, write_cursor, size(), lost_element_count(),
            std::vector<T>(container.begin(), container.end()));
    }

    template<typename T, std::size_t Size>
    std::size_t ring_buffer<T, Size>::size() const {
      return mod(static_cast<std::size_t>(write_cursor - read_cursor), Size);
    }

    template<typename T, std::size_t Size>
    std::size_t ring_buffer<T, Size>::lost_element_count() {
      std::lock_guard <std::mutex> lock(read_mutex);

      int64_t w = write_cursor, r = read_cursor, s = Size;
      int64_t lost = MAX(w - r - s, 0);
      lost_data += lost;
      read_cursor += lost;

      return lost_data;
    }

}