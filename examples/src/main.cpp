#include <iostream>
#include <ring_buffer.hpp>
#include <csignal>

int main() {
  nextlib::ring_buffer<int, 5> buffer {};
  for (int i = 0; i < 80; i++) {
    buffer.push(i + 1);
  }
  buffer.debug();
  for (uint i = 0; i < 3; i++)
    std::cout << "Val: " << buffer.pop_get().value_or(-1) << std::endl;
  buffer.debug();

  nextlib::ring_buffer<sig_atomic_t, 5> volatile_buffer {};
  volatile_buffer.push(static_cast<volatile sig_atomic_t>(3));

  return 0;
}
